module Monitor.HistoryTable
( buildHistoryTable
, initHistoryTable
, writeHistory
) where

import           Data.List                (intercalate)
import           Data.Maybe               (fromJust)
import           Database.HDBC            (SqlValue, commit, executeMany,
                                           getTables, prepare, run, toSql)
import           Database.HDBC.PostgreSQL (Connection)
import           Monitor.Const
import           Monitor.Utils            (expandDims)

buildHistoryTable :: Config -> HistoryTable
buildHistoryTable config = do
  let source = sourceTable config

  let timeCols = fromJust $ expandDims $ resolution config
  let eventCols = ["monitor", "event_type"]
  let types = (replicate (length timeCols) "INT") ++
              (replicate (length eventCols) "VARCHAR(128)")

  HistoryTable { sourceForT = source,
                 columns = timeCols ++ eventCols,
                 colTypes = types }


historyTableName :: String -> String
historyTableName source = source++"__ecg_history"


historyTableToSql :: HistoryTable -> String
historyTableToSql t = unlines [header, cols, pk, footer]
  where
    header = "CREATE TABLE "++(historyTableName (sourceForT t))++"("
    cols = concat $ zipWith (\x y -> x++" "++y++",\n") (columns t) (colTypes t)
    pk = "PRIMARY KEY("++(intercalate ", " (columns t))++")"
    footer = ");"


initHistoryTable :: Connection -> HistoryTable -> IO ()
initHistoryTable db historyTable = do
  let tableName = historyTableName $ sourceForT historyTable
  tables <- getTables db
  if elem tableName tables -- TODO check schema
    then putStrLn $ "table found: "++tableName++"\
                    \ ((W) no schema checks implemented yet)"
    else do
      let sql = historyTableToSql historyTable
      run db sql []
      commit db
      putStrLn $ "table created: "++tableName


-- writeback

eventToSql :: Event -> [SqlValue]
eventToSql e = dates ++ values
  where
    dates = date e
    values = map toSql [base e, show (eType e)]

-- TODO check schema
writeHistory :: Connection -> HistoryTable -> [Event] -> IO ()
writeHistory _ _ [] = return ()
writeHistory db historyTable events = do
  let name = historyTableName $ sourceForT historyTable
  let eventSql = map eventToSql events
  let width = length $ head eventSql -- TODO validate rectangularity
  let args = intercalate ", " $ replicate width "?"
  stmt <- prepare db $ "INSERT INTO "++name++" VALUES ("++args++")\
                       \ ON CONFLICT DO NOTHING"
  executeMany stmt eventSql
  commit db
