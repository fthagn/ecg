module Monitor.Utils where

import           Database.HDBC            (SqlValue, fromSql, quickQuery')
import           Database.HDBC.PostgreSQL (Connection, connectPostgreSQL)
import           Monitor.Const

connectDb :: String -> IO Connection
connectDb connString = connectPostgreSQL connString


expandDims :: String -> Maybe [String]
expandDims "day"   = Just ["year", "month", "day"]
expandDims "week"  = Just ["year", "week"]
expandDims "month" = Just ["year", "month"]
expandDims "year"  = Just ["year"]
expandDims _       = Nothing


monitorToSql :: Monitor -> String
monitorToSql m = ag++"("++c0++") AS "++ag++"_"++c1
  where
    ag = aggregation m
    c0 = column m
    c1 = if (c0 == " * ")
      then "all"
      else c0


-- TODO check if custom-implementing show breaks the yaml-parser
monitorShow :: Monitor -> String
monitorShow m = ag++"_"++c1
  where
    ag = aggregation m
    c0 = column m
    c1 = if (c0 == " * ")
      then "all"
      else c0


timeDimToSql :: String -> String -> String
timeDimToSql ts dim = "EXTRACT("++dim++" FROM "++ts++")::int AS "++dim


-- TODO revisit
-- TODO also, technically the case "database was changed and then changed back
-- -> before function finished" is not covered either...
checksum :: Connection -> Config -> IO String
checksum db config = do
  let sql = "SELECT md5(CAST((array_agg(f.*)) AS text))\
            \ FROM "++(sourceTable config)++" f;"
  result <- quickQuery' db sql []
  let md5 = fromSql $ head $ head result
  return md5


unwrap :: (String, [(Bool, Bool)]) -> [(String, EventType, [Bool])]
unwrap c = [(name, Exceeded, exceeded), (name, Subceeded, subceeded)]
  where
    name = fst c
    values = snd c
    exceeded = map fst values
    subceeded = map snd values


colToEvents :: [[SqlValue]]  -> (String, EventType, [Bool]) -> [Event]
colToEvents allDates (base, eType, values) = events
  where
    zipped = zip values allDates
    filtered = filter fst zipped
    dates = map snd filtered

    dateToEvent = Event base eType
    events = map dateToEvent dates
