module Monitor.Monitor
( monitorCheck
) where

import           Data.List                (intercalate, transpose)
import           Data.Maybe               (fromJust)
import           Database.HDBC            (SqlValue, executeRaw, fetchAllRows,
                                           fetchAllRowsAL, fromSql, prepare)
import           Database.HDBC.PostgreSQL (Connection)
import           Monitor.Const
import           Monitor.SourceView       (sourceViewName)
import           Monitor.Utils            (checksum, colToEvents, expandDims,
                                           monitorShow, unwrap)

colCheck :: (String, [SqlValue]) -> (String, [(Bool, Bool)])
colCheck col = let
  name = fst col
  values = map (fromSql :: SqlValue -> Double) (snd col)
  sums = scanl1 (+) values
  n = (map fromInteger [1..])
  means = zipWith (/) sums n

  squares = zipWith (\v m -> (v-m)*(v-m)) values means
  sumSquares = scanl1 (+) $ squares
  vars = zipWith (/) sumSquares n
  stdevs = map ((fromInteger limitStdevs * ) . sqrt) vars

  upper = zipWith (+) means stdevs
  lower = zipWith (-) means stdevs

  exceeded = zipWith (>) values upper
  subceeded = zipWith (<) values lower

  result = zip exceeded subceeded
  in (name, result)


columnParse :: [[(String, SqlValue)]] -> Maybe [(String, [SqlValue])]
columnParse rows = if (verifyNames cols)
  then Just $ map extractColumn cols
  else Nothing
  where
    cols = transpose rows

    extractName :: [(String, SqlValue)] -> String
    extractName c = fst $ head c

    extractNames :: [(String, SqlValue)] -> [String]
    extractNames c = map fst c

    verifyName :: [(String, SqlValue)] -> Bool
    verifyName c = foldr1 (&&) namesChecked
      where
        name = extractName c
        equalsName = (==) name
        names = extractNames c
        namesChecked = map equalsName names

    verifyNames :: [[(String, SqlValue)]] -> Bool
    verifyNames cols = foldr1 (&&) (map verifyName cols)

    extractValues :: [(String, SqlValue)] -> [SqlValue]
    extractValues c = map snd c

    extractColumn :: [(String, SqlValue)] -> (String, [SqlValue])
    extractColumn c = (extractName c, extractValues c)


monitorCheck :: Connection -> Config -> IO [Event]
monitorCheck db config = do
  preChecksum <- checksum db config
  let view = sourceViewName $ sourceTable config

  let m = intercalate ", " $ map monitorShow $ monitors config
  let sql = "SELECT "++m++" FROM "++view++";"
  getValues <- prepare db sql
  executeRaw getValues
  valueRows <- fetchAllRowsAL getValues
  if null valueRows
    then do
      putStrLn "(W) source view returned nothing (should not happen normally)"
      return []
    else do
      let valueCols = fromJust $ columnParse valueRows
      let checked = map colCheck valueCols
      let unwrapped = concat $ map unwrap checked

      let dateCols = fromJust $ expandDims $ resolution config
      let dateColSql = intercalate ", " dateCols
      getDates <- prepare db $ "SELECT "++dateColSql++" FROM "++view
      executeRaw getDates
      dateRows <- fetchAllRows getDates

      let nValues = length valueRows
      let nDates = length dateRows

      if nValues /= nDates
        then do
          putStrLn $ "incoherent lengths for values ("++(show nValues)++")\
                     \ and dates ("++(show nDates)++")"
          putStrLn "retrying"
          monitorCheck db config -- TODO limit iterations with config arg
        else do
          let events = concat $ map (colToEvents dateRows) unwrapped

          postChecksum <- checksum db config

          if preChecksum /= postChecksum
            then do
              putStrLn "source was changed while processing, repeating checks"
              monitorCheck db config -- TODO limit iterations with config arg
            else return events
