module Monitor.SourceView
( buildSourceView
, sourceViewName
, initSourceView
) where

import           Data.List                (intercalate)
import           Data.Maybe               (fromJust)
import           Database.HDBC            (commit, run)
import           Database.HDBC.PostgreSQL (Connection)
import           Monitor.Const
import           Monitor.Utils            (expandDims, monitorToSql,
                                           timeDimToSql)

buildSourceView :: Config -> SourceView
buildSourceView config = do
  let source = sourceTable config

  let timeDims = fromJust $ expandDims $ resolution config
  let timeCols = map (timeDimToSql (tsColumn config)) timeDims
  let monitorCols = map monitorToSql $ monitors config

  SourceView { sourceForV = source,
               columnSql = timeCols ++ monitorCols,
               groupBys = timeDims,
               orderBys = timeDims }


sourceViewName :: String -> String
sourceViewName source = source++"__ecg_stats"


sourceViewToSql :: SourceView -> String
sourceViewToSql v = unlines ["CREATE OR REPLACE VIEW\
                             \ "++(sourceViewName (sourceForV v)),
                             "AS SELECT "++(intercalate ", " (columnSql v)),
                             "FROM "++(sourceForV v),
                             "GROUP BY "++(intercalate ", " (groupBys v)),
                             "ORDER BY "++(intercalate ", " (orderBys v)),
                             ";"]


initSourceView :: Connection -> SourceView -> IO ()
initSourceView db sourceView = do
  let sql = sourceViewToSql sourceView
  run db sql []
  commit db
