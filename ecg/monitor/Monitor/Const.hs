{-# LANGUAGE DeriveGeneric #-}

module Monitor.Const where

import           Data.Yaml     (FromJSON)
import           Database.HDBC (SqlValue)
import           GHC.Generics

data ConnArgs = ConnArgs  { host     :: String,
                            dbname   :: String,
                            user     :: String,
                            password :: String
                          }
instance Show ConnArgs where
  show c = unwords ["host="++(host c),
                    "dbname="++(dbname c),
                    "user="++(user c),
                    "password="++(password c)]

demoDbConnArgs = ConnArgs { host = "source_demo_db",
                            dbname = "pagila",
                            user = "root",
                            password = "admin"
                          }

historyDbConnArgs = ConnArgs { host = "history_db",
                               dbname = "db",
                               user = "user",
                               password = "secret"
                             }
limitStdevs = 1

data Monitor = Monitor  { column      :: String,
                          aggregation :: String
                        } deriving (Eq, Show, Generic)
data Config = Config    { sourceTable :: String,
                          tsColumn    :: String,
                          resolution  :: String,
                          monitors    :: [Monitor]
                        } deriving (Eq, Show, Generic)
instance FromJSON Monitor
instance FromJSON Config

data SourceView = SourceView { sourceForV :: String,
                               columnSql  :: [String],
                               groupBys   :: [String],
                               orderBys   :: [String]
                             }

data HistoryTable = HistoryTable { sourceForT :: String,
                                   columns    :: [String],
                                   colTypes   :: [String]
                                 }

data EventType = Exceeded | Subceeded
instance Show EventType where
  show Exceeded  = "exceeded previous mean by >"++(show limitStdevs)++" stdevs"
  show Subceeded = "subceeded previous mean by >"++(show limitStdevs)++" stdevs"
data Event = Event { base  :: String,
                     eType :: EventType,
                     date  :: [SqlValue]
                   } deriving Show
