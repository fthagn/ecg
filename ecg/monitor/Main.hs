-- {-# LANGUAGE DeriveGeneric #-}

module Main where

import           Data.Yaml            (FromJSON, decodeFileThrow)
import           Database.HDBC        (disconnect)
import           Monitor.Const
import           Monitor.HistoryTable (buildHistoryTable, initHistoryTable,
                                       writeHistory)
import           Monitor.Monitor      (monitorCheck)
import           Monitor.SourceView   (buildSourceView, initSourceView)
import           Monitor.Utils        (connectDb)


main :: IO ()
main = do
  putStrLn "loading config"
  config <- decodeFileThrow "config.yml" :: IO Config -- TODO escape everything
  putStrLn "connecting to source-db"
  source_db <- connectDb $ show demoDbConnArgs
  putStrLn "connecting to history-db"
  history_db <- connectDb $ show historyDbConnArgs
  putStrLn "initializing source view"
  let sourceView = buildSourceView config
  initSourceView source_db sourceView
  putStrLn "processing"
  events <- monitorCheck source_db config
  if null events
    then putStrLn "no events found"
    else do
      let numEvents = length events
      putStrLn $ show numEvents++" events found"
      putStrLn "initializing history table"
      let historyTable = buildHistoryTable config
      initHistoryTable history_db historyTable
      putStrLn "writing results"
      writeHistory history_db historyTable events

  putStrLn "disconnecting"
  disconnect source_db
  disconnect history_db
  putStrLn "done"
