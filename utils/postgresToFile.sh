#!/usr/bin/env bash

container="7d8aa9f97101"
db="pagila"
source="payment__ecg_stats"

sql="COPY (SELECT * FROM ${source}) TO STDOUT WITH CSV HEADER"

parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )

out="${parent_path}/../csv/${source}.csv"

docker exec ${container} psql -d ${db} -c "${sql}" > ${out}
