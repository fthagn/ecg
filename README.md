# ecg: A monitor for whatever data has for a pulse
Right now that would be the deviation from the (running) mean, measured in multiples of standard deviation.

![](analysis/sum_amount.png)
![](analysis/count_all.png)
![](analysis/Screenshot.png)

## how to run demo:

* git clone https://gitlab.com/fthagn/ecg.git
* cd ecg/ecg
* (optional: play with ./monitor/config.yml, which specifies the targets to surveil)
* docker-compose up -d
* in the container running monitor_hs: cabal run :monitor (this will execute the checks once and write the results to history_db)
* in the container running history_db you can now use psql to inspect the detected events (screenshots above)

If you have questions or would like to contribute please let me know.
